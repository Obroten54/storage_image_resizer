package main

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/h2non/bimg"
)

func imageProcessing(buffer []byte, dirname string, baseName string, quality int, res string) (string, error) {
	filename := dirname + "/" + baseName + ".webp"

	if res != "full" {
		resOptions := strings.Split(res, "x")
		width, err := strconv.Atoi(resOptions[0])
		if err != nil {
			return filename, err
		}
		height, err := strconv.Atoi(resOptions[1])
		if err != nil {
			return filename, err
		}
		buffer, err = bimg.NewImage(buffer).ResizeAndCrop(width, height)
		if err != nil {
			return filename, err
		}
	}

	converted, err := bimg.NewImage(buffer).Convert(bimg.WEBP)
	if err != nil {
		return filename, err
	}

	processed, err := bimg.NewImage(converted).Process(bimg.Options{Quality: quality})
	if err != nil {
		return filename, err
	}

	writeError := bimg.Write(filename, processed)
	if writeError != nil {
		return filename, writeError
	}

	return filename, nil
}

func fileNameWithoutExtension(fileName string) string {
	return strings.TrimSuffix(filepath.Base(fileName), filepath.Ext(fileName))
}

func getOriginalPrefix(path string) string {
	pathExploded := strings.Split(path, "/")
	prefix := ""
	for i, s := range pathExploded {
		if i < len(pathExploded)-2 && s != "" {
			prefix += s + "/"
		}
	}
	nameExploded := strings.Split(pathExploded[len(pathExploded)-1], ".")
	namePrefix := ""
	for i := 0; i < len(nameExploded)-1; i++ {
		namePrefix += nameExploded[i]
	}
	return prefix + namePrefix
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func keyExists(sess *session.Session, bucket string, key string) bool {
	_, err := s3.New(sess).HeadObject(&s3.HeadObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	})
	if err != nil {
		if ansErr, ok := err.(awserr.Error); ok {
			switch ansErr.Code() {
			case "NotFound":
				return false
			default:
				return false
			}
		}
		return false
	}
	return true
}

func getClosestRes(res string) string {
	if res == "full" {
		return res
	}

	screens := map[string][]string{
		"landscape": strings.Split(os.Getenv("LANDSCAPE_RESOLUTIONS"), ","),
		"portrait":  strings.Split(os.Getenv("PORTRAIT_RESOLUTIONS"), ","),
	}
	options := strings.Split(res, "x")
	w, _ := strconv.Atoi(options[0])
	h, _ := strconv.Atoi(options[1])
	format := "landscape"
	if h > w {
		format = "portrait"
	}
	if stringInSlice(res, screens[format]) {
		return res
	}

	for _, screen := range screens[format] {
		s := strings.Split(screen, "x")
		sw, _ := strconv.Atoi(s[0])
		sh, _ := strconv.Atoi(s[1])
		if sw >= w && sh <= h {
			return screen
		}
	}

	return "full"
}
