package main

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/gofiber/fiber/v2"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

func main() {
	app := fiber.New()

	app.Get("/", func(c *fiber.Ctx) error {
		path := c.Query("path", "")
		if path == "" {
			return c.Status(fiber.StatusNotFound).SendString("File not found!")
		}

		var sess *session.Session
		if os.Getenv("AWS_USE_ENDPOINT") == "true" {
			sess = session.Must(session.NewSession(&aws.Config{
				Endpoint: aws.String(os.Getenv("AWS_ENDPOINT")),
			}))
		} else {
			sess = session.Must(session.NewSession())
		}

		originalPrefix := getOriginalPrefix(path)
		resp, _ := s3.New(sess).ListObjects(&s3.ListObjectsInput{
			Bucket: aws.String(os.Getenv("AWS_BUCKET")),
			Prefix: aws.String(originalPrefix),
		})
		if len(resp.Contents) == 0 || len(resp.Contents) > 1 {
			fmt.Println(fmt.Errorf("filed to find original file by prefix %q", originalPrefix))
			return c.Status(fiber.StatusNotFound).SendString("File not found!")
		}
		originalPath := resp.Contents[0].Key

		pathExploded := strings.Split(path, "/")
		requiredRes := pathExploded[len(pathExploded)-2]
		closestRes := "full"
		if requiredRes != "full" {
			closestRes = getClosestRes(requiredRes)
		}
		closestResKey := strings.Replace(path, requiredRes, closestRes, 1)
		if keyExists(sess, os.Getenv("AWS_BUCKET"), closestResKey) {
			return c.Redirect(os.Getenv("AWS_URL")+closestResKey, 301)
		}

		fileName := filepath.Base(*originalPath)
		tmpPath := "/tmp/" + fileName
		d, err := os.Create(tmpPath)
		if err != nil {
			fmt.Println(fmt.Errorf("failed to create file %q, %v", tmpPath, err))
			return c.Redirect(os.Getenv("AWS_URL") + "/" + *originalPath)
		}

		downloader := s3manager.NewDownloader(sess)
		_, err = downloader.Download(d, &s3.GetObjectInput{
			Bucket: aws.String(os.Getenv("AWS_BUCKET")),
			Key:    originalPath,
		})
		if err != nil {
			fmt.Println(fmt.Errorf("failed to download file, %v", err))
			return c.Redirect(os.Getenv("AWS_URL") + "/" + *originalPath)
		}

		buffer, err := os.ReadFile(tmpPath)
		if err != nil {
			fmt.Println(fmt.Errorf("failed to read file, %v", err))
			return c.Redirect(os.Getenv("AWS_URL") + "/" + *originalPath)
		}
		quality, err := strconv.Atoi(os.Getenv("QUALITY"))
		newFileName, err := imageProcessing(buffer, "/tmp", fileNameWithoutExtension(*originalPath), quality, closestRes)
		if err != nil {
			fmt.Println(fmt.Errorf("failed to process file, %v", err))
			return c.Redirect(os.Getenv("AWS_URL") + "/" + *originalPath)
		}

		uploader := s3manager.NewUploader(sess)
		u, err := os.Open(newFileName)
		if err != nil {
			fmt.Println(fmt.Errorf("failed to open processed file %q, %v", newFileName, err))
			return c.Redirect(os.Getenv("AWS_URL") + "/" + *originalPath)
		}

		newKey := ""
		for i, s := range pathExploded {
			if i < len(pathExploded)-2 && s != "" {
				newKey += s + "/"
			}
		}
		newKey += closestRes + "/" + filepath.Base(newFileName)
		_, err = uploader.Upload(&s3manager.UploadInput{
			Bucket: aws.String(os.Getenv("AWS_BUCKET")),
			Key:    aws.String(newKey),
			Body:   u,
		})
		if err != nil {
			fmt.Println(fmt.Errorf("failed to upload file %q, %v", newFileName, err))
			return c.Redirect(os.Getenv("AWS_URL") + "/" + *originalPath)
		}

		d.Close()
		u.Close()
		os.Remove(tmpPath)
		os.Remove(newFileName)

		return c.Redirect(os.Getenv("AWS_URL")+"/"+newKey, 301)
	})

	app.Listen(":3000")

	defer app.Shutdown()
}
