module gitlab.com/Obroten54/storage_image_resizer

go 1.19

require (
	github.com/aws/aws-sdk-go v1.44.176
	github.com/gofiber/fiber/v2 v2.41.0
	github.com/google/uuid v1.3.0
	github.com/h2non/bimg v1.1.9
)

require (
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/joho/godotenv v1.4.0 // indirect
	github.com/klauspost/compress v1.15.9 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.43.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/sys v0.3.0 // indirect
)
