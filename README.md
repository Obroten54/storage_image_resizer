# Docker processor for resizing and optimizing images in bucket

App for resizing, optimizing images and converting images to webp-format on s3 bucket

### Schema

**First GET request:**

![](source/images/schema1.webp "First GET request"){: .shadow}

**Subsequent requests:**

![](source/images/schema2.webp "Subsequent requests"){: .shadow}

### Installation

Clone repository to your server:
```
git clone https://gitlab.com/Obroten54/storage_image_resizer.git
```
Copy and rename `env.example` to `.env`, and specify the app settings in `.env`

After this go to repository folder and start the server:
```
cd storage_image_resizer
docker compose up
```

## Bucket configuration
```
s3.putBucketWebsite({
    Bucket: "%bucket_name%",
    WebsiteConfiguration: {
        IndexDocument: {
            Suffix: "index.html"
        },
        RoutingRules: [
            {
                Condition: {
                    HttpErrorCodeReturnedEquals: "404",
                    KeyPrefixEquals: "image/"
                },
                Redirect: {
                    HttpRedirectCode: "302",
                    HostName: "example.net",
                    Protocol: "https",
                    ReplaceKeyPrefixWith: "?path=image/",
                }
            }
        ]
    }
})
```